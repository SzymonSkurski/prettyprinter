package prettyprinter

import (
	"encoding/json"
	"fmt"

	"github.com/TwiN/go-color"
)

// Pretty Print
func PP(pt any) {
	m, _ := json.MarshalIndent(pt, "", "\t")
	fmt.Println(string(m))
}

// pretty print multiple arguments
func PP2(args ...any) {
	fmt.Print(string(color.Blue))
	for _, arg := range args {
		argFormatted, _ := json.MarshalIndent(arg, "", "\t")
		fmt.Print(string(argFormatted), " ")
	}
	fmt.Println(string(color.Reset))
}

func printInColor(c string, args ...any) {
	fmt.Print(string(c))
	for _, arg := range args {
		PP(arg)
	}
	fmt.Println(string(color.Reset))
}

func Var_dump(expression ...interface{}) {
	fmt.Printf("\n%#v\n", expression)
}

func PrintRed(args ...any) {
	printInColor(color.Red, args...)
}

func PrintPurple(args ...any) {
	printInColor(color.Purple, args...)
}

func PrintGreen(args ...any) {
	printInColor(color.Green, args...)
}

func PrintBlue(args ...any) {
	printInColor(color.Blue, args...)
}

func PrintYellow(args ...any) {
	printInColor(color.Yellow, args...)
}
